/**
 * Created with Webohjelmisto.
 * User: ME-karkkaal
 * Date: 2015-12-11
 * Time: 08:19 AM
 * To change this template use Tools | Templates.
 */
describe('Hakeminen', function () {
    beforeEach(function () {
        var osoiteteksti = document.createElement("p");
        osoiteteksti.setAttribute("id", "osoiteteksti");
        var varoitus = document.createElement("p");
        varoitus.setAttribute("id", "varoitus");
        var taulukkoDiv = document.createElement("div");
        taulukkoDiv.setAttribute("id", "taulukkoDiv");
        var karttaDiv = document.createElement("div");
        karttaDiv.setAttribute("id", "map");
        var testiDiv = document.createElement("div");
        testiDiv.setAttribute("id", "testiDiv");
        testiDiv.appendChild(osoiteteksti);
        testiDiv.appendChild(varoitus);
        testiDiv.appendChild(taulukkoDiv);
        testiDiv.appendChild(karttaDiv);
        document.body.appendChild(testiDiv);
    });
    afterEach(function () {
        var testiDiv = document.getElementById("testiDiv");
        testiDiv.parentNode.removeChild(testiDiv);
    });

    it('Taulukko piirtyy', function () {
        var pysakki = {
            name: "Testinimi",
            address: "Testiosoite",
            dist: 100
        };
        var taulukko = [pysakki];
        expect(document.getElementById("taulukkoDiv")).toBeDefined();
        teeTaulukko(taulukko);
        expect(document.getElementsByTagName("tr")).toBeDefined();
    });
    it('Virheellinen haku tulostaa ilmoitustekstin virheestä', function () {
        vaaraSijainti();
        expect(document.getElementById("varoitus").innerHTML).toBe("Virheellinen haku");
    });
    it('httpGet metodi palauttaa tavaraa', function () {
        var jsonPalautus = JSON.parse(httpGet("http://api.reittiopas.fi/hsl/prod/?request=geocode&user=hannuro&pass=lollero&epsg_out=4326&format=text&key=" + "Eira"));
        expect(jsonPalautus[0].name).toBe("Eira");
    });
	it('Elementti tyhjenee', function() {
		var paragraafi = document.createElement("p");
		paragraafi.setAttribute("id", "testiP");		
		document.getElementById("testiDiv").appendChild(paragraafi);
		var elementti = document.getElementById("testiP");
		console.log(document.getElementById("testiP"));
		console.log(document.getElementById("testiDiv"));
		elementti.innerHTML = "Testiä";		
		expect(elementti.innerHTML).toBe('Testiä');
		tyhjennaElementti('testiP');
		expect(elementti.innerHTML).toBe('');
	});
});
describe('Google maps', function () {
    beforeEach(function () {
        var testiDiv = document.createElement("div");
        testiDiv.setAttribute("id", "testiDiv");
        var karttaDiv = document.createElement("div");
        karttaDiv.setAttribute("id", "map");
        testiDiv.appendChild(karttaDiv);
        document.body.appendChild(testiDiv);
    });
    afterEach(function () {
        var testiDiv = document.getElementById("testiDiv");
        testiDiv.parentNode.removeChild(testiDiv);
    });
    it('Kartta piirtyy', function () {
        var sijainti = "26.1209971,65.059616";
		siirraSijainti(sijainti);
		expect(typeof getKartta()).toBe("object");
		expect(getKartta().center).toBeDefined();
    });


});