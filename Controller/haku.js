/**
 * Created with WebProjektiR1.
 * User: hannuro
 * Date: 2015-12-02
 * Time: 09:29 AM
 * To change this template use Tools | Templates.
 */
/* global window, console, document, XMLHttpRequest, google, navigator, $ */
"use strict";
//Kun klikataan hakukenttää, tyhjennetään edellinen haku
var haku = document.getElementById("haku");
haku.addEventListener("click", function () {
    haku.value = "";
});
//Metodi hakee tiedot parametrina annetusta osoitteesta.
//Käytetään Google Maps, Google Maps Geolocation ja Reittiopas API
/**
 * Metodi jolla haetaan dataa XMLHTTPRequestin avulla
 * @param {string} theURl - osoite josta data haetaan
 */

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}
//Kun klikataan hae nappia, suoritetaan haku 
var button = document.getElementById("hae");
var osoiteteksti = document.getElementById("osoiteteksti");
var haku = document.getElementById("haku");
button.onclick = function () {
    submitHaku(haku);
};
/**
 * Metodi jolla katsotaan onko entriä painettu
 * jos on niin suoritetaan haku
 * @param {event} event - tapahtuma
 */

function onkoEntteri(event) {
    if(event.which == 13 || event.keyCode == 13) {
        submitHaku(haku);
        return false;
    }
    return true;
}
/**
 * Tyhjentää elementin sisällön
 * @param {string} id - Tyhjennettävän elementin id
 */

function tyhjennaElementti(id) {
    document.getElementById(id).innerHTML = "";
}
//Metodi ottaa käyttäjän hakuparametrin ja hakee sillä osuvimman osoitteen tiedot
/**
 * Metodi jolla haetaan osoite Reittiopas APIsta
 * @param {string} haku - Katuosoite tai paikan nimi, jolla sijaintia haetaan
 * @return {Boolean} True, jos haku onnistui, muuten false.
 *
 */

function submitHaku(haku) {
    tyhjennaElementti("osoiteteksti");
    var osoite = "http://api.reittiopas.fi/hsl/prod/?request=geocode&user=hannuro&pass=lollero&epsg_out=4326&format=text&key=" + haku.value;
    var loydetytOsoitteet;
    var lahtopiste;
    var onnistuikoHaku = true;
    try { //Yritetään hakea osoitetta
        var xmlpalautus = httpGet(osoite);
        loydetytOsoitteet = JSON.parse(xmlpalautus);
        lahtopiste = loydetytOsoitteet[0];	
        osoiteteksti.innerHTML = "Haulla löytyi osoite:  " + lahtopiste.matchedName + ", kaupunki: " + lahtopiste.city + ".";
        var varoitus = document.getElementById("varoitus");
        tyhjennaElementti("varoitus");
        haeAlue(lahtopiste);
    } catch(err) { //Jos osoitetta ei löydy tai virheellinen haku
        tyhjennaElementti("osoiteteksti");
        tyhjennaElementti("taulukkoDiv");
        vaaraSijainti();
        onnistuikoHaku = false;
    }
    return onnistuikoHaku;
}
//Haetaan alueen pysäkit osoitteen perusteella
/**
 * Metodi jolla haetaan pysäkit 500m säteellä
 * @param {string} osoite - osoite jonka avulla haetaan lähimmät pysäkit
 */

function haeAlue(osoite) {
    var pysakit;
    var cords = osoite.coords;
    var haeAlueKomento = "http://api.reittiopas.fi/hsl/prod/?request=stops_area&user=hannuro&pass=lollero&epsg_in=4326&epsg_out=4326&format=json&center_coordinate=" + cords + "&diameter=500";
    var xmlpalautus = httpGet(haeAlueKomento);
    pysakit = JSON.parse(xmlpalautus);
    teeTaulukko(pysakit);
    siirraSijainti(cords);
}
//Metodi antaa varoituksen että tulosta ei löytynyt, tyhjentää taulukon ja laittaa uuden sijainnin
/**
 * Metodi jolla annetaan varoitus jos osoitetta ei löytynyt tai haku oli väärä
 */

function vaaraSijainti() {
    var varoitus = document.getElementById("varoitus");
    varoitus.innerHTML = "Virheellinen haku";
    var pituusaste = 26.1209971;
    var leveysaste = 65.059616;
    var sijainti = {
        lat: leveysaste,
        lng: pituusaste
    };
    console.log("Pituus: " + pituusaste + " leveys: " + leveysaste);
    //Luodaan uusi kartta ja laitetaan se haetun paikan koordinaatteihin
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: leveysaste,
            lng: pituusaste
        },
        zoom: 15
    });
    var infoWindow = new google.maps.InfoWindow({
        map: map
    });
    //Merkkaa missä sijainti on
    infoWindow.setPosition(sijainti);
    infoWindow.setContent("Hevonperse, Oulu");
    map.setCenter(sijainti);
}
//Metodi luo taulukon saaduista hakutuloksista
/**
 * Metodi luo taulukon sivulle, jossa on löydetyt pysäkit
 * @param {obj} taulukko - taulukko jossa on pysäkkien tiedot
 */

function teeTaulukko(taulukko) {
    console.log("Aloitettiin taulukon luonti: ");
    console.log(taulukko);
    var taulukkoDiv = document.getElementById("taulukkoDiv");
    console.log("Div löytyi: " + taulukkoDiv);
    taulukkoDiv.innerHTML = "";
    var table = document.createElement("table");
    table.setAttribute("class", "table table-hover");
    table.setAttribute("id", "datatable");
    //Otsikot
    var otsikot = table.createTHead();
    var riviH = otsikot.insertRow(0);
    var nimiH = riviH.insertCell(0);
    nimiH.innerHTML = "Nimi";
    var osoiteH = riviH.insertCell(1);
    osoiteH.innerHTML = "Osoite";
    var matkaH = riviH.insertCell(2);
    matkaH.innerHTML = "Matka (m)";
    var tRunko = table.createTBody();
    for(var i = 0; i < taulukko.length; i++) {
        (function () {
            var pysakki = taulukko[i];
            var rivi = tRunko.insertRow(i);
            rivi.addEventListener("click", function () {
                siirraMerkki(pysakki);
            }, false);
            var nimi = rivi.insertCell(0);
            nimi.innerHTML = pysakki.name;
            var osoite = rivi.insertCell(1);
            osoite.innerHTML = pysakki.address;
            var matka = rivi.insertCell(2);
            matka.innerHTML = taulukko[i].dist;
        }());
    }
    taulukkoDiv.appendChild(table);
    $('#datatable').DataTable({
        dom: 'Bfrtip',
        bFilter: false,
        bInfo: false,
        bPaginate: false,
        scroller: false,
        scrollY: false,
        paging: false
    });
}
//
// Google Maps Web avain : AIzaSyBezlreopuv7nUlJQID51ClRBWj-SPVw6I
// 
var map;

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ? 'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
}
//Metodi siirtää Google Maps karttaa saatujen koordinaattien pisteisiin ja lisää
// markerin kyseiseen sijaintiin
/**
 * Metodi jolla siirretään Google Maps kartta oikeaan sijaintiin
 * @param {float[]} koordinaatit - koordinaatit joihin kartta siirretään
 */

function siirraSijainti(koordinaatit) {
    koordinaatit = koordinaatit.split(",");
    var pituusaste = parseFloat(koordinaatit[0]);
    var leveysaste = parseFloat(koordinaatit[1]);
    var sijainti = {
        lat: leveysaste,
        lng: pituusaste
    };
    console.log("Pituus: " + pituusaste + " leveys: " + leveysaste);
    //Luodaan uusi kartta ja laitetaan se haetun paikan koordinaatteihin
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: leveysaste,
            lng: pituusaste
        },
        zoom: 15
    });
    //Merkkaa missä sijainti on
	siirraGoogleMerkki(map, sijainti);
    map.setCenter(sijainti);
}

function siirraMerkki(pysakki) {
    siirraSijainti(pysakki.coords);
}

/**
 * Siirtää Google Maps -merkin paikkaa
 * @param {obj} kartta - johon merkki piirretään
 * @param {float[]} sijainti - koordinaatit joihin merkki siirretään
 */
function siirraGoogleMerkki(kartta, sijainti) {
    var marker = new google.maps.Marker({
        position: sijainti,
        map: kartta,
        title: "Pysäkki"
    });
}

function getKartta() {
	return map;
}